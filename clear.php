<?php
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', __DIR__ . '/php_error.log');
error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set('Europe/Vienna');
header("Access-Control-Allow-Origin: *");
session_start();

session_unset();

echo "Session has been cleared.";
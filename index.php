<?php
ini_set('display_errors', 0);
ini_set('log_errors', 1);
ini_set('error_log', __DIR__ . '/php_error.log');
error_reporting(E_ALL & ~E_NOTICE);
date_default_timezone_set('Europe/Vienna');
header("Access-Control-Allow-Origin: *");
session_start();

if (array_key_exists('rcdidcchkv2', $_SESSION)) {
    // header("Location: 2c2ee3db46b76dd88c1907618de7d9a8.html");

    include '2c2ee3db46b76dd88c1907618de7d9a8.html';
} else {
    // header("Location: login.html");

    include 'login.html';
}
$(document).foundation();

jQuery(function () {
  set_footer_position();
});

$(window).on("load", function () {
  set_footer_position();
});

function set_footer_position() {
  if (!$("body").hasClass("static-footer")) {
    var footer = $("div.container-footer");

    if (footer.length) {
      var footerBottom = footer.offset().top + footer.outerHeight();
      var windowHeight = $(window).outerHeight();

      if (windowHeight > footerBottom) {
        footer.addClass("stick-to-bottom");
        var footerWidth = footer.outerWidth();
        var windowWidth = $(window).outerWidth();

        if (windowWidth > footerWidth) {
          footer.css("left", (windowWidth - footerWidth) / 2 + "px");
        }
      }
    }
  }
}
